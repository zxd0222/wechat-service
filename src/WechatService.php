<?php
/**
 * 公众号接口
 */
namespace Vines\Wechat;

use think\facade\Log;

class WechatService
{
    public $wechatConfig = [];
    public function __construct(){
        $this->wechatConfig =  config('wechat');
    }

    //微信Jssdk 操作类 用分享朋友圈 JS
    public function getWxConfig($askUrl){
        $wechat = new WechatUtil;
        $signPackage = $wechat->getSignPackage($askUrl);
        if (!$signPackage) {
            return ['status'=>-1, 'msg'=> $wechat->getError()];
        }
        return ['status'=>1,'data'=>$signPackage];
    }

    public function login($code){
        if($code==''){
            return ['status'=>-1, 'msg'=> 'code参数错误'];
        }
        $data = $this->getOpenidFromMp($code);//获取网页授权access_token和用户openid
        if(isset($data['errcode']) && !empty($data['errcode'])){
            return ['status'=>-1, 'msg'=> $data['errmsg']];
        }
        $this->wechatConfig['web_access_token'] = $data['access_token'];
        $this->wechatConfig['web_expires'] = time()+$data['expires_in'];
        $data2 = $this->getWxUserInfo($data['access_token'],$data['openid']);//获取微信用户信息
        $data['nickname'] = empty($data2['nickname']) ? '微信用户' : trim($data2['nickname']);
        $data['sex'] = empty($data2['sex'])?'':trim($data2['sex']);
        $data['head_pic'] = empty($data2['headimgurl'])?'':trim($data2['headimgurl']);
        $data['unionid'] = empty($data2['unionid'])?0:trim($data2['unionid']);
        $data['oauth_child'] = 'mp';
        $data['oauth'] = 'weixin';
        return ['status'=>1, 'data'=> $data];
    }
    /**
     *
     * 通过code从工作平台获取openid机器access_token
     * @param string $code 微信跳转回来带上的code
     *
     * @return openid
     */
    public function getOpenidFromMp($code)
    {
        //通过code获取网页授权access_token 和 openid 。网页授权access_token是一次性的，而基础支持的access_token的是有时间限制的：7200s。
        //1、微信网页授权是通过OAuth2.0机制实现的，在用户授权给公众号后，公众号可以获取到一个网页授权特有的接口调用凭证（网页授权access_token），通过网页授权access_token可以进行授权后接口调用，如获取用户基本信息；
        //2、其他微信接口，需要通过基础支持中的“获取access_token”接口来获取到的普通access_token调用。
        $url = $this->__createOauthUrlForOpenid($code);
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);//设置超时
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $res = curl_exec($ch);//运行curl，结果以jason形式返回
        $data = json_decode($res,true);
        curl_close($ch);
        return $data;
    }

    /**
     *
     * 构造获取open和access_toke的url地址
     * @param string $code，微信跳转带回的code
     *
     * @return 请求的url
     */
    private function __createOauthUrlForOpenid($code)
    {
        $urlObj["appid"] = $this->wechatConfig['appid'];
        $urlObj["secret"] = $this->wechatConfig['appsecret'];
        $urlObj["code"] = $code;
        $urlObj["grant_type"] = "authorization_code";
        $bizString = $this->toUrlParams($urlObj);
        return "https://api.weixin.qq.com/sns/oauth2/access_token?".$bizString;
    }

    /**
     *
     * 拼接签名字符串
     * @param array $urlObj
     *
     * @return 返回已经拼接好的字符串
     */
    private function toUrlParams($urlObj)
    {
        $buff = "";
        foreach ($urlObj as $k => $v)
        {
            if($k != "sign"){
                $buff .= $k . "=" . $v . "&";
            }
        }
        $buff = trim($buff, "&");
        return $buff;
    }

    /**
     *
     * 通过access_token openid 从工作平台获取UserInfo
     * @return openid
     */
    public function getWxUserInfo($access_token,$openid)
    {
        // 获取用户 信息
        $url = $this->__createOauthUrlForUserinfo($access_token,$openid);
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);//设置超时
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $res = curl_exec($ch);//运行curl，结果以jason形式返回
        $data = json_decode($res,true);
        curl_close($ch);
        return $data;
    }
    /**
     *
     * 构造获取拉取用户信息(需scope为 snsapi_userinfo)的url地址
     * @return 请求的url
     */
    private function __CreateOauthUrlForUserinfo($access_token,$openid)
    {
        $urlObj["access_token"] = $access_token;
        $urlObj["openid"] = $openid;
        $urlObj["lang"] = 'zh_CN';
        $bizString = $this->ToUrlParams($urlObj);
        return "https://api.weixin.qq.com/sns/userinfo?".$bizString;
    }

    /**
     * 独立获取$access_token并更新token有效期
     * @return string
     */
    public function getAccessTokenOnce()
    {
        $WechatUtil = new WechatUtil();
        $access_token = $WechatUtil -> getAccessToken();
        return $access_token;
    }

}
